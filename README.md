# docker-compose-networking-example

You can just take a look at the actual docker-compose files in this repository, but since I already started a writeup below, it's there too.

The examples are: caddy fronting heimdall.

Another alternative is to add a `container_name` to the master container and do the same as here: https://p.miosa.me/miosame/docker-openvpn-clientdocker-openvpn-client#user-content-using-with-other-containers

xxxxxxxxxx

Say we have two docker-compose files, `master/docker-compose.yml` and `slave/docker-compose.yml` inside the master you want to setup a network for the slave to join into:

At the end of the master file:

```
networks:
    your_net:
        ipam:
            driver: default
            config:
                - subnet: 172.20.0.0/16
```

The `subnet` and `your_net` are configurable to your needs.

Now inside the slave docker-compose, every service that should join the network, can either be joined via static IP (handy for e.g. nginx/caddy proxying):

```
    networks:
      default:
          ipv4_address: 172.20.0.31
```

and at the end of the slave docker-compose have the network definition as:

```
networks:
  default:
    external:
      name: container-name_your_net
```

you can easily check the `name` necessary by doing `docker network ls` and see the full name your `your_net` got assigned. (usually just )

or if you don't care about its IP being assigned (why bother with this whole setup then?..) you can just not assign it a `ipv4_address` like this:

```
    networks:
      default:
```

and it'll join the network, but with a randomly assigned ipv4 that's available.